package com.sistema.testapps.datasource

import androidx.recyclerview.widget.DiffUtil

data class Item(
    val id: String,
    val name: String,

    //Addition
    var selected: Boolean = false
    ) {

    companion object {

        val DIFF_UTIL = object : DiffUtil.ItemCallback<Item>() {

            override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem.id == newItem.id
            }

        }

    }

}
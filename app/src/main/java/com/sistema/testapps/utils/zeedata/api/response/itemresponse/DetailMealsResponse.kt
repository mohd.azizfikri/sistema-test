package com.sistema.testapps.utils.zeedata.api.response.itemresponse

import com.google.gson.annotations.SerializedName
import com.sistema.testapps.utils.zeedata.api.response.itemresponse.part.DetailMealsData

data class DetailMealsResponse(
    @field:SerializedName("meals")
    val meals: List<DetailMealsData>
)
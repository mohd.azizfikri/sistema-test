package com.sistema.testapps.utils.zeedata.exception

class UnexpectedRepositoryResultException(message: String = "Unexpected error occured."): Exception(message)
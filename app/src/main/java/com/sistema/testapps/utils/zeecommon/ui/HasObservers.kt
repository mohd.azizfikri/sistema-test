package com.sistema.testapps.utils.zeecommon.ui

interface HasObservers {

    fun setupObservers()

}
package com.sistema.testapps.utils.zeedata.api.response.itemresponse

import com.google.gson.annotations.SerializedName
import com.sistema.testapps.utils.zeedata.api.response.itemresponse.part.MealsData

data class MealsResponse(
    @field:SerializedName("meals")
    val meals: List<MealsData>
)
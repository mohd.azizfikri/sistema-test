package com.sistema.testapps.utils.zeedata.exception

class FailureResultException(message: String) : Exception(message)
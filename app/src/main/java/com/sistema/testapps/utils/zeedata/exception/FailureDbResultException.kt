package com.sistema.testapps.utils.zeedata.exception

class FailureDbResultException(message: String) : Exception(message)
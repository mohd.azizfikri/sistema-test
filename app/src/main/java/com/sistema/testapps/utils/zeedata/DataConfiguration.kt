package com.sistema.testapps.utils.zeedata

import com.sistema.testapps.utils.zeedata.db.AppDatabase
import com.sistema.testapps.utils.zeedata.prefs.AppPrefs

data class DataConfiguration(
    val remoteHost: String = "",
    val remoteTimeout: Long = 30,
    val databaseName: String = AppDatabase.DEFAULT_DATABASE_NAME,
    val prefsName: String = AppPrefs.DEFAULT_PREFS_NAME,
    val prefsAlwaysCommit: Boolean = false,
    val debug: Boolean = true
)
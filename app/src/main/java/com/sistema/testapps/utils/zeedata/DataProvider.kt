package com.sistema.testapps.utils.zeedata

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.sistema.testapps.utils.zeedata.api.ApiService
import com.sistema.testapps.utils.zeedata.db.AppDatabase
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object DataProvider {

    private lateinit var mApplicationContext: Application
    private lateinit var mDataConfiguration: DataConfiguration

    private lateinit var mApiService: ApiService
    private lateinit var mAppDatabase: AppDatabase

    private val mDefaultPreferences by lazy {
        getContext().getSharedPreferences(
            getConfig().prefsName,
            Context.MODE_PRIVATE
        )
    }

    @Synchronized
    fun init(app: Application, dataConfiguration: DataConfiguration = DataConfiguration()) {
        mApplicationContext = app
        mDataConfiguration = dataConfiguration

        mAppDatabase = provideAppDatabase(app, dataConfiguration)
        mApiService =
            provideRetrofit(dataConfiguration).create(ApiService::class.java)
    }

    fun getContext(): Context {
        return mApplicationContext
    }

    fun getConfig(): DataConfiguration {
        return mDataConfiguration
    }

    fun getPrefs(): SharedPreferences {
        return mDefaultPreferences
    }

    fun getApiService(): ApiService {
        return mApiService
    }

    fun getAppDatabase(): AppDatabase {
        return mAppDatabase
    }

    /**
     * Retrofit provider
     */
    private fun provideRetrofit(config: DataConfiguration): Retrofit {
        return Retrofit
            .Builder()
            .baseUrl(config.remoteHost)
            .addConverterFactory(GsonConverterFactory.create())
            .client(provideRetrofitClient(config))
            .build()
    }

    private fun provideRetrofitClient(config: DataConfiguration): OkHttpClient {
        return OkHttpClient()
            .newBuilder()
            .addInterceptor(provideHttpLoggingInterceptor(config))
            .writeTimeout(config.remoteTimeout, TimeUnit.SECONDS)
            .readTimeout(config.remoteTimeout, TimeUnit.SECONDS)
            .build()
    }

    private fun provideHttpLoggingInterceptor(config: DataConfiguration): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = if (config.debug) HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.NONE
        }
    }

    /**
     * Room provider
     */
    private fun provideAppDatabase(context: Context, config: DataConfiguration): AppDatabase {
        return Room
            .databaseBuilder(context, AppDatabase::class.java, config.databaseName)
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }

}
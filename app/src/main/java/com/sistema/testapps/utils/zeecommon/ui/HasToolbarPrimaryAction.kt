package com.sistema.testapps.utils.zeecommon.ui

import android.view.View

interface HasToolbarPrimaryAction {

    val onToolbarPrimaryAction: (v: View) -> Unit

}
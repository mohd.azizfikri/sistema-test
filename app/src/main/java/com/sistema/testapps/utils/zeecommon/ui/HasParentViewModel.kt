package com.sistema.testapps.utils.zeecommon.ui

interface HasParentViewModel {

    val parentViewModel: AppViewModel

}
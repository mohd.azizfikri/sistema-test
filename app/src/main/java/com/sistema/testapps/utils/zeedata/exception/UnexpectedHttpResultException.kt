package com.sistema.testapps.utils.zeedata.exception

import com.sistema.testapps.utils.zeedata.api.response.general.MessageResponse

class UnexpectedHttpResultException(errorResponse: MessageResponse) :
    Exception(errorResponse.message ?: errorResponse.error)
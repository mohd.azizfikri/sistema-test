package com.sistema.testapps.utils.zeecommon.ui

interface HasBindings {

    fun setupBinding()

}
package com.sistema.testapps.utils.zeedata.api.response.general

import com.google.gson.annotations.SerializedName

/**
 * Data of lists
 */
data class DataListResponse<T>(
    @field:SerializedName("totalResults")
    val totalResults: Int = 0,

    @field:SerializedName("status")
    val statusDesc: String = "",

    @field:SerializedName(
        "data",
        alternate = ["articles"])
    val data: List<T>? = null
)
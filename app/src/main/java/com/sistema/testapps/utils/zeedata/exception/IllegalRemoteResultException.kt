package com.sistema.testapps.utils.zeedata.exception

class IllegalRemoteResultException(message: String) : Exception(message)
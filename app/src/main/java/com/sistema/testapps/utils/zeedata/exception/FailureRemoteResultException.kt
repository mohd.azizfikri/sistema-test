package com.sistema.testapps.utils.zeedata.exception

class FailureRemoteResultException(message: String) : Exception(message)
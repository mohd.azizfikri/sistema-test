package com.sistema.testapps.utils.zeedata.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sistema.testapps.utils.zeedata.db.dao.*
import com.sistema.testapps.utils.zeedata.db.entity.Configuration

@Database(
    entities = [
        Configuration::class,
    ],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun configurationDao(): ConfigurationDao

    companion object {
        const val DEFAULT_DATABASE_NAME = "APP_DB"
    }

}
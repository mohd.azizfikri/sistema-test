package com.sistema.testapps.utils.zeedata.exception.error

class IncompleteSignInInformationError(message: String? = null) : Exception(message)
package com.sistema.testapps.utils.zeecommon.ui

interface HasToolbarTitle {

    val toolbarTitle: String

}
package com.sistema.testapps.utils.zeecommon

data class SelectableData<T>(
    var selected: Boolean = false,
    val origin: T
)
package com.sistema.testapps.utils.zeedata.api.response.itemresponse.part

import com.google.gson.annotations.SerializedName

data class MealsData(
    @field:SerializedName("idMeal")
    val idMeal: String,

    @field:SerializedName("strMeal")
    val strMeal: String,

    @field:SerializedName("strMealThumb")
    val strMealThumb: String
)
package com.sistema.testapps.utils.zeecommon.ui

interface HasBackPressedConfirmation {

    val backPressDelay: Long

}
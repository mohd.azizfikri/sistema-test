package com.sistema.testapps.utils.zeedata.api

import com.sistema.testapps.utils.zeedata.api.response.itemresponse.*
import com.sistema.testapps.utils.zeedata.api.response.general.DataResponse
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    /**
     * MEALS ===============================================================================
     */

    @GET("api/json/v1/1/filter.php")
    suspend fun getMealsList(
        @Query("c") kind: String = "Seafood"
    ): Response<MealsResponse>

    @GET("api/json/v1/1/lookup.php")
    suspend fun getDetailMeals(
        @Query("i") idMeals: String? = null,
    ): Response<DetailMealsResponse>

}
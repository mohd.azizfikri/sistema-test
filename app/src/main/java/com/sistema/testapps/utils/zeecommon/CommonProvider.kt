package com.sistema.testapps.utils.zeecommon

object CommonProvider {

    private var mViewModelBindingId: Int? = null

    fun init(viewModelBindingId: Int) {
        mViewModelBindingId = viewModelBindingId
    }

    fun getViewModelBindingId() = mViewModelBindingId

}
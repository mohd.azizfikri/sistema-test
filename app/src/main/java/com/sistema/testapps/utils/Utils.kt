package com.sistema.testapps.utils

import android.content.Context
import android.widget.DatePicker
import android.widget.Toast
import com.sistema.testapps.datasource.Item
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

fun listCountry(): ArrayList<Item>{
    val sample: ArrayList<Item> = ArrayList()
    sample.add(Item("br","Brazil"))
    sample.add(Item("fr","France"))
    sample.add(Item("de","Germany"))
    sample.add(Item("id","Indonesia"))
    sample.add(Item("jp","Japan"))
    sample.add(Item("my","Malaysia"))
    sample.add(Item("ae","United Arab Emirates"))
    sample.add(Item("us","United States"))
    return sample
}

fun listCategory(): ArrayList<Item>{
    val sample: ArrayList<Item> = ArrayList()
    sample.add(Item("business","Bussiness"))
    sample.add(Item("entertainment","Entertainment"))
    sample.add(Item("general","General"))
    sample.add(Item("health","Health"))
    sample.add(Item("science","Science"))
    sample.add(Item("sports","Sports"))
    sample.add(Item("technology","Technology"))
    return sample
}

fun Context.toast(text: String) {
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}

package com.sistema.testapps.utils.zeecommon.ui

interface HasViews {

    fun setupViews()

}
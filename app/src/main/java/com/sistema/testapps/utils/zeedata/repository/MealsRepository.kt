package com.sistema.testapps.utils.zeedata.repository

import com.natpryce.*
import com.sistema.testapps.utils.zeedata.api.response.itemresponse.part.DetailMealsData
import com.sistema.testapps.utils.zeedata.api.response.itemresponse.part.MealsData
import com.sistema.testapps.utils.zeedata.util.AppRepository

class MealsRepository : AppRepository() {

    suspend fun getListMeals(
        keyword: String = "Seafood"
    ): Result<List<MealsData>, Exception> {
        val result = asDirectResult { getApi().getMealsList(keyword) }.onFailure {
            return it
        }
        return Success(result.meals)
    }

    suspend fun getDetailMeals(
        idMeals: String
    ): Result<DetailMealsData, Exception> {
        val result = asDirectResult { getApi().getDetailMeals(idMeals) }.onFailure {
            return it
        }
        return Success(result.meals.first())
    }
}
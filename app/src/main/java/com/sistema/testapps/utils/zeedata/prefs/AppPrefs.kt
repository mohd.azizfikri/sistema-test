package com.sistema.testapps.utils.zeedata.prefs

import android.annotation.SuppressLint
import android.content.SharedPreferences
import com.sistema.testapps.utils.zeedata.DataProvider

abstract class AppPrefs {

    protected val prefs by lazy { DataProvider.getPrefs() }
    protected val useCommit by lazy { DataProvider.getConfig().prefsAlwaysCommit }

    fun clearAll() {
        edit { clear() }
    }

    @SuppressLint("ApplySharedPref")
    private fun edit(call: SharedPreferences.Editor.() -> Unit) {
        prefs.edit().apply {
            call()
            if (useCommit) commit() else apply()
        }
    }

    companion object {
        const val DEFAULT_PREFS_NAME = "APP_PREFS"
    }

}
package com.sistema.testapps.feature.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.natpryce.Success
import com.natpryce.onFailure
import com.sistema.testapps.common.adapter.MealsListAdapter
import com.sistema.testapps.utils.zeecommon.SelectableData
import com.sistema.testapps.utils.zeecommon.extension.toFalse
import com.sistema.testapps.utils.zeecommon.ui.AppViewModel
import com.sistema.testapps.utils.zeecommon.util.event.LiveEvent
import com.sistema.testapps.utils.zeecommon.util.event.MutableLiveEvent
import com.sistema.testapps.utils.zeecommon.util.list.AppDataWrapper
import com.sistema.testapps.utils.zeedata.api.response.itemresponse.part.MealsData
import com.sistema.testapps.utils.zeedata.repository.MealsRepository
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
    private val mealsRepository: MealsRepository
) : AppViewModel(), MealsListAdapter.ArticleListener {
    val mealsListWrapper by lazy { AppDataWrapper<SelectableData<MealsData>>() }

    private val _openMealsDetailEvent by lazy { MutableLiveEvent<MealsData>() }
    val openMealsDetailEvent: LiveEvent<MealsData> = _openMealsDetailEvent

    init {
        fetch()
    }

    fun fetch() {
        viewModelScope.launch {
            mealsListWrapper.load {
                val results = mealsRepository.getListMeals().onFailure {
                    loading.toFalse()
                    return@load Success(emptyList())
                }
                loading.toFalse()
                Success(results.map { SelectableData(origin = it) })
            }
        }
    }

    override fun onClickArticle(data: MealsData) {
        _openMealsDetailEvent.set(data)
    }
}
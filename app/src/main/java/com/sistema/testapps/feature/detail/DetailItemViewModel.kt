package com.sistema.testapps.feature.detail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.natpryce.onFailure
import com.sistema.testapps.utils.zeecommon.extension.toFalse
import com.sistema.testapps.utils.zeecommon.ui.AppViewModel
import com.sistema.testapps.utils.zeecommon.util.event.LiveEvent
import com.sistema.testapps.utils.zeecommon.util.event.MutableLiveEvent
import com.sistema.testapps.utils.zeedata.api.response.itemresponse.part.DetailMealsData
import com.sistema.testapps.utils.zeedata.api.response.itemresponse.part.MealsData
import com.sistema.testapps.utils.zeedata.repository.MealsRepository
import kotlinx.coroutines.launch

class DetailItemViewModel @ViewModelInject constructor(
    private val mealsRepository: MealsRepository
) : AppViewModel() {
    val mealsDetail by lazy { MutableLiveData<DetailMealsData>() }
    private val idMeals by lazy { MutableLiveData<String>() }

    private val _openYoutubeAppsEvent by lazy { MutableLiveEvent<String>() }
    val openYoutubeAppsEvent: LiveEvent<String> = _openYoutubeAppsEvent
    
    fun setup(id: String){
        idMeals.value = id
        fetch()
    }

    fun fetch(){
        viewModelScope.launch {
            val result = mealsRepository.getDetailMeals(idMeals.value.orEmpty()).onFailure {
                loading.toFalse()
                return@launch
            }
            mealsDetail.value = result
        }
    }
    
    fun openYoutube(){
        when(mealsDetail.value?.strYoutube){
            "" -> return
            else -> _openYoutubeAppsEvent.set(mealsDetail.value?.strYoutube.orEmpty())
        }
    }
}
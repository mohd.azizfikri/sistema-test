package com.sistema.testapps.feature.detail

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.activity.viewModels
import com.sistema.testapps.R
import com.sistema.testapps.databinding.ActivityDetailItemBinding
import com.sistema.testapps.utils.zeecommon.ui.AppActivity
import com.sistema.testapps.utils.zeecommon.ui.HasObservers
import com.sistema.testapps.utils.zeecommon.ui.HasViews
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class DetailItemActivity : AppActivity<ActivityDetailItemBinding, DetailItemViewModel>
    (R.layout.activity_detail_item), HasViews, HasObservers {
    override val viewModel by viewModels<DetailItemViewModel>()

    private val mExtraIdContent by lazy { intent.getStringExtra(EXTRA_ID_CONTENT) }

    override fun setupViews() {
        viewModel.setup(mExtraIdContent.orEmpty())

        viewBinding.ltoolbar.setOnPrimaryIconClick { finish() }
    }

    @SuppressLint("QueryPermissionsNeeded")
    override fun setupObservers() {
        viewModel.openYoutubeAppsEvent.observe(this) {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(it)
            )
            intent.component = ComponentName(
                "com.google.android.youtube",
                "com.google.android.youtube.PlayerActivity"
            )

            val infos = packageManager.queryIntentActivities(intent, 0)
            if (infos.size > 0) {
                startActivity(intent)
            } else {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(it)
                startActivity(intent)
            }
        }
    }

    companion object {
        const val EXTRA_ID_CONTENT = "EXTRA_ID_CONTENT"
        fun launch(activity: Activity, content: String){
            activity.startActivity(Intent(activity, DetailItemActivity::class.java).apply {
                putExtra(EXTRA_ID_CONTENT, content)
            })
        }
    }

}
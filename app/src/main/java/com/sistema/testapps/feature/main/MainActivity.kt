package com.sistema.testapps.feature.main

import androidx.activity.viewModels
import com.sistema.testapps.R
import com.sistema.testapps.common.adapter.MealsListAdapter
import com.sistema.testapps.databinding.ActivityMainBinding
import com.sistema.testapps.feature.detail.DetailItemActivity
import com.sistema.testapps.utils.zeecommon.ui.AppActivity
import com.sistema.testapps.utils.zeecommon.ui.HasObservers
import com.sistema.testapps.utils.zeecommon.ui.HasViews
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppActivity<ActivityMainBinding, MainViewModel>
    (R.layout.activity_main), HasViews, HasObservers {

    override val viewModel by viewModels<MainViewModel>()

    private val mMealsListAdapter by lazy { MealsListAdapter(viewModel) }

    override fun setupViews() {
        viewBinding.rvMeals.adapter = mMealsListAdapter
    }

    override fun setupObservers() {
        viewModel.openMealsDetailEvent.observe(this) {
            DetailItemActivity.launch(this, it.idMeal)
        }
    }
}
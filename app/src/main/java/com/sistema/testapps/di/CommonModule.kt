package com.sistema.testapps.di

import android.content.Context
import com.sistema.testapps.utils.ResourceManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext

@Module
@InstallIn(ApplicationComponent::class)
object CommonModule {

    @Provides
    fun resourceManager(@ApplicationContext context: Context): ResourceManager {
        return ResourceManager(context)
    }

}
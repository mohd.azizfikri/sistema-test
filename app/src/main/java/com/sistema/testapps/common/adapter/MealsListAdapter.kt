package com.sistema.testapps.common.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.sistema.testapps.databinding.ItemListMealsBinding
import com.sistema.testapps.utils.zeecommon.SelectableData
import com.sistema.testapps.utils.zeecommon.util.list.AppRecyclerView
import com.sistema.testapps.utils.zeedata.api.response.itemresponse.part.MealsData

class MealsListAdapter(
    private val mListener: ArticleListener
) : AppRecyclerView<ItemListMealsBinding, SelectableData<MealsData>>(DIFF_UTIL) {

    override fun onCreateViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): ItemListMealsBinding = ItemListMealsBinding.inflate(inflater, parent, false)

    override fun onPrepareBindViewHolder(
        binding: ItemListMealsBinding,
        model: SelectableData<MealsData>
    ) {
        binding.data = model.origin

        binding.llArticle.setOnClickListener {
            mListener.onClickArticle(model.origin)
        }
    }

    interface ArticleListener {
        fun onClickArticle(data: MealsData)
    }

    companion object {

        val DIFF_UTIL = object : DiffUtil.ItemCallback<SelectableData<MealsData>>() {

            override fun areItemsTheSame(
                oldItem: SelectableData<MealsData>,
                newItem: SelectableData<MealsData>
            ): Boolean {
                return oldItem.origin == newItem.origin
            }

            override fun areContentsTheSame(
                oldItem: SelectableData<MealsData>,
                newItem: SelectableData<MealsData>
            ): Boolean {
                return oldItem.origin.idMeal == newItem.origin.idMeal
            }

        }
    }

}
package com.sistema.testapps

import android.app.Application
import com.sistema.testapps.utils.Util
import com.sistema.testapps.utils.zeecommon.CommonProvider
import com.sistema.testapps.utils.zeedata.DataConfiguration
import com.sistema.testapps.utils.zeedata.DataProvider
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CoreApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        Util.init(this)
        CommonProvider.init(BR.viewmodel)
        DataProvider.init(
            this, DataConfiguration(
                remoteHost = "https://www.themealdb.com/",
                remoteTimeout = 60L,
                debug = BuildConfig.DEBUG
            )
        )
    }
}